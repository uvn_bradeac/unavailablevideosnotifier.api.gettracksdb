const AWS = require('aws-sdk')
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'})

exports.handler = (event, context, callback) => {
    let playlistId = event.query.playlistid
    let tracks = []

    getData(playlistId)

    let getData = (playlistId) => {
        const params = {
            Key: {
                "playlistId": {
                    S: playlistId
                }
            }, 
            TableName: "playlistItems"
        }

        dynamodb.getItem(params, (err, data) => {
            if (err) {
                console.log('Error. Getting item(s) from dynamodb failed: ', JSON.stringify(err, null, 2))
                callback(err, null)
            }
            else {
                try {
                    if (data.Item) {
                        tracks = JSON.parse(data.Item.tracks.S)
                        callback(null, tracks)
                    }
                    
                    callback(null, [])
                }
                catch(e) {
                    if (data.Item.tracks.S.length > 0) {
                        tracks = data.Item.tracks.S
                        callback(null, tracks)
                    }
                    callback(e, null)
                }
            }
        })
    }
}